import React from 'react'
import JobList from './JobList'

class JobForm extends React.Component {

    constructor() {
        super()

        this.state = {
            jobs: [],
            priorityValue: 'Urgent',
            textValue: '',
            counter: 0
        }

        this.handleCreate = this.handleCreate.bind(this);
        this.onTextInputChange = this.onTextInputChange.bind(this);
        this.onSelectChange = this.onSelectChange.bind(this);
    }



    handleCreate(event) {
        event.preventDefault()
        this.state.jobs.push(
            {
                priorityValue: this.state.priorityValue,
                textValue: this.state.textValue,
                id: this.state.counter,
                colours: {
                    Urgent: 'red',
                    Regular: 'yellow',
                    Trivial: 'LightBlue'
                }
            }
        )
        let c = this.state.counter
        this.setState({
            counter: c + 1,
            textValue: '',
            priorityValue: 'Urgent'
        })

        console.log(this.state.jobs)

    }

    onTextInputChange(event) {
        let value = event.target.value
        value = value.replace(/[^A-Za-z]/ig, '')

        this.setState({ textValue: value })
    }

    onSelectChange(event) {
        this.setState({ priorityValue: event.target.value })
    }

    handleEdit = (editJob) => {
        let jobs = this.state.jobs

        for (const i in jobs) {
            if (jobs[i].id === editJob.id) {
                jobs[i].priorityValue = editJob.priority
                break
            }
        }
    }

    handleDelete = (id) => {
        let jobs = this.state.jobs

        for (const i in jobs) {
            if (jobs[i].id === id) {
                jobs.splice(i, 1)
                this.setState({ jobs: jobs })
                break
            }
        }
    }

    render() {
        return (
            <div style={{ width: '95%', margin: 'auto' }}>
                <form onSubmit={this.handleCreate} required>
                    <div>
                        <div>
                            <label>
                                Job:
                <br />
                                <input type="text" name="job" placeholder="Job"
                                    value={this.state.textValue} onChange={this.onTextInputChange} style={{ width: '97.5%', padding: '15px', borderRadius: '5px' }} required
                                    maxLength="70" />
                            </label>
                        </div>
                        <br />
                        <label>
                            Priority:
                <br />
                            <select value={this.state.priorityValue} name="priority" onChange={this.onSelectChange} 
                            style={{ width: '100%', padding: '15px', borderRadius: '5px'}}>
                                <option value="Urgent">Urgent</option>
                                <option value="Regular">Regular</option>
                                <option value="Trivial">Trivial</option>
                            </select>
                        </label>
                    </div>
                    <div style={{ paddingTop: '20px' }}>
                        <input type="submit" name="Create" value="Create"  ></input>
                    </div>
                </form>

                <JobList jobs={this.state.jobs} edit={this.handleEdit} delete={this.handleDelete} />
            </div>
        )
    }
}

export default JobForm