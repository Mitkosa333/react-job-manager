import React from 'react'
import EditForm from './EditForm'

const JOB_LIST_STYLES = {
    display: 'grid',
    gridTemplateColumns: 'repeat(3, 1fr)',
    padding: '20px'
}
const JOB_TEXT_STYLES = {
    padding: '20px', fontWeight: 'bold', fontSize: '20px'
}

class JobList extends React.Component {

    state = {
        filter: '',
        jobToShow: {},
        isOpen: false,
    }


    updateSearch(event) {
        this.setState({ filter: event.target.value })
    }

    onEditClick(job) {
        this.setState({
            isOpen: true,
            jobToShow: job
        })
    }

    onCloseHandle = () => {
        this.setState({ isOpen: false })
    }

    handleEdit = (editJob) => {
        this.props.edit(editJob)
    }

    handleDelete = (id) => {
        this.props.delete(id)
    }

    sort(jobs) {
        const urgentJobs = []
        const regularJobs = []
        const trivialJobs = []

        for (const i in jobs) {
            if (jobs[i].priorityValue === 'Urgent') {
                urgentJobs.push(jobs[i])
            } else if (jobs[i].priorityValue === 'Regular') {
                regularJobs.push(jobs[i])
            } else if (jobs[i].priorityValue === 'Trivial') {
                trivialJobs.push(jobs[i])
            }
        }

        const result = urgentJobs.concat(regularJobs, trivialJobs)
        return result
    }

    render() {

        const filteredJobs = this.props.jobs.filter((job) => { return job.textValue.toLowerCase().indexOf(this.state.filter) !== -1 })

        const filteredSortedJobs = this.sort(filteredJobs)

        return (
            <div>
                <div style={{
                    display: 'flex',
                    justifyContent: 'space-between'
                }}>
                    <h1 style={{ fontSize: '20px' }}>Job List</h1>
                    <input size='8' type="text" placeholder="Search Job" value={this.state.filter} onChange={this.updateSearch.bind(this)} style={{ float: 'right', textAlign: 'center', fontSize: '24px' }}></input>
                </div>
                <br />
                <hr style={{
                    backgroundColor: 'black',
                    height: '5px'
                }} />
                <div>
                    {
                        filteredSortedJobs.map((job) =>
                            <ul key={job.id} style={{ backgroundColor: job.colours[job.priorityValue], margin: 'auto' }}>
                                <div style={JOB_LIST_STYLES}>
                                    <div style={JOB_TEXT_STYLES}>{job.textValue}</div>
                                    <div style={JOB_TEXT_STYLES}> {job.priorityValue}</div>
                                    <div style={{ padding: '20px' }}>
                                        <button onClick={() => this.onEditClick(job)}>Edit</button>
                                        <div style={{
                                            width: '5px',
                                            height: 'auto',
                                            display: 'inline-block'
                                        }} />
                                        <button onClick={() => this.handleDelete(job.id)}>Delete</button>
                                    </div>
                                </div>
                            </ul>
                        )
                    }
                </div>
                <EditForm isOpen={this.state.isOpen} onClose={this.onCloseHandle} onEdit={this.handleEdit} job={this.state.jobToShow} />
            </div>
        )
    }
}


export default JobList