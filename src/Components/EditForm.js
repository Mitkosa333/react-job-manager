import React from 'react'
import ReactDom from 'react-dom'

const MODAL_STYLES = {
    position: 'fixed',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    backgroundColor: '#FFF',
    padding: '3% 10%',
    zIndex: 1000,
    borderStyle: 'solid',
    borderRadius: '15px'
}

class EditForm extends React.Component {

    state = {
        isOpen: this.props.isOpen,
        editedJob: {
            id: this.props.job.id,
            priority: this.props.job.priorityValue
        }
    }

    handleCancel = () => {
        this.props.onClose()
    }

    handleSubmit = () => {
        this.props.onEdit(this.state.editedJob)
        this.props.onClose()
    }

    onSelectChange = (event) => {
        this.setState({
            editedJob: {
                priority: event.target.value,
                id: this.props.job.id
            }
        })
    }

    render() {
        if (!this.props.isOpen) {
            return null
        }

        return ReactDom.createPortal(
            <div>
                <div style={MODAL_STYLES}>
                    <div onClick={this.handleCancel}
                        style={{ padding: '15px', position: 'absolute', top: '0px', right: '0px', margin: '0px', color: '#C0C0C0', userSelect:'none' }}>x</div>
                    <div style={{
                        display: 'grid',
                        padding: '10px'
                    }}>
                        <div style={{
                            padding: '20px',
                            textAlign: 'center'
                        }}>
                            {this.props.job.textValue}
                        </div>
                        <select style={{
                            padding: '5px 100px',
                            margin: '20px'
                        }} value={this.state.editedJob.priority} name="priority" onChange={this.onSelectChange}>
                            <option value="Urgent">Urgent</option>
                            <option value="Regular">Regular</option>
                            <option value="Trivial">Trivial</option>
                        </select>
                        <div style={{ padding: '5px', textAlign: 'center' }}>
                            <button style={{
                                textAlign: 'center'
                            }} onClick={this.handleSubmit}>Update</button>
                        </div>
                    </div>
                </div>
            </div>
            ,
            document.getElementById('modal')
        )
    }
}

export default EditForm