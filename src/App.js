import React from 'react';
import './App.css';
import JobForm from './Components/JobForm'

function App() {
  return (
    <div>
      <JobForm />
    </div>
  );
}

export default App;
